<?php

header('content-type:application/json; charset=utf-8');
$approximatif = "%" . $_GET["recherche"] . "%";
$precision = $_GET["precision"];
try {
    $json = NULL;
    $bdd = new PDO('mysql:host=developont.fr;dbname=developonteur;charset=utf8mb4', 'developonteur', 'G4rdeP0n!');
    $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $req = $bdd->prepare('SELECT * FROM musique WHERE artiste LIKE :artiste OR album LIKE :album OR titre LIKE :titre LIMIT 10');
    if (isset($_GET['recherche'])) {
        
        if ($precision == "exact") {
        $req->execute(array(
        'artiste' => $_GET['recherche'], 
        'album' => $_GET['recherche'],
        'titre' => $_GET['recherche']
        ));
        
        } else {
            $req->execute(array(
            'artiste' => $approximatif,
            'album' => $approximatif,
            'titre' => $approximatif
        ));     
        }

        $req->setFetchMode(PDO::FETCH_ASSOC);
        while (($musique = $req->fetchAll())) {
            $json = json_encode($musique);
        }
    }
} catch (PDOException $e) {
    printf('Erreur de connexion : %s\n', $e->getMessage());
}

echo $json;
?>
