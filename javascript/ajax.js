$(function() {

    $body = $("body");
    
    $('#remove').click(function() {
        $(".printResults").empty();
    });
 

  $('#Rechercher').click(function() {


    var keyWords = $("#mots_cles").val();   // Résultat du champ de recherche + récupération de la valeur de l'index selectionné (select HTML).
    var select = document.getElementById("search");
    var selectIndex = select.selectedIndex;
    var selectChoice = select.options[selectIndex].value;
      
    var selectPrecision = document.getElementById("precision");
    var selectIndexPrecision = selectPrecision.selectedIndex;
    var selectPrecisionChoice = selectPrecision.options[selectIndexPrecision].value;

    $.getJSON('processing.php?recherche=' + keyWords + '&precision=' + selectPrecisionChoice, function(data) {})
        .done(function(data){

        document.getElementsByClassName("modal")[0].style.backgroundColor = "orange";

        var results = data;
        
        if(data == null) {
            
            document.getElementsByClassName("modal")[0].style.backgroundColor = "red";
            
        } else {
         
            var n = results.length;
            var i = 0;
            var printResults = String();

            if(data != "") {

                    $(".printResults").remove();

                }    


            if(selectChoice == "tous") {

                $("#resultat").append("<ul class='printResults'>");

                while (i < n) { //<-- Affiche tous les résultats de la recherche

                    $("#resultat").append("<li  class='printResults'>Album:" + results[i].album + "</li>" + //<-- Suppression de <ul> ayant pour ID = #printResults.
                                          "<li  class='printResults'>Artiste:" + results[i].artiste + "</li>" +
                                          "<li  class='printResults'>Titre:" + results[i].titre + "</li>");
                    i++;

                }

                $("#resultat").append("</ul>");

            } else {
                $("#resultat").append("<ul class='printResults'>");
                while (i < n) { //<-- Affiche seulement les résultats de la liste selectionné.

                    $("#resultat").append("<li  class='printResults'>" + results[i][selectChoice] + "</li>");
                    i++;
                }
                $("#resultat").append("</ul>");
            }
            
            document.getElementsByClassName("modal")[0].style.backgroundColor = "green";
        }
        
        })
            
        
        .fail(function(data) {

        document.getElementsByClassName("modal")[0].style.backgroundColor = "red";
        
        })
        .always(function(data) {
        
        
        })
     //<-- Requête AJAX qui renvoi le GET et qui reçoit les données (data) de API PHP (processing.php).

  });
});
